# 1. Project management and documentation

    In this first module, we set up the project management and the documentation process.

## 1. Creating an SSH key

    I personally skipped this part because, in my opinion, it was unnecessary. The GitLab repository is public which means it can be cloned without any authentication method and when it comes to pushing changes to the remote, it's just a matter of simple authentication. The main reason behind this choice is that I already have some specific SSH keys for different private and non-private git servers. Sometimes I work with tokens because they're permission driven. I didn't want to create an additional one, as this repository won't require thousands of commits and pushes anyway.



For those who are interested in knowing how to generate and use a SSH key. Some good documentation is provided [here](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent).



However, it is important to note that the -t in the following command




```shell
$ ssh-keygen -t ed25519 -C "comment to differentiate between keys"
```


indicates the type of the algorithm that's being used to generate the key. Also, in the example given by Github they suggest to use your email (after -C) but, you can put whatever you want. The purpose is to be able to identify your keys with the help of a custom comment.



## 2. Cloning the repository

    I already possess the entire toolchain for git so I didn't have to install anything.

I had to clone the repository in order to modify it.

It was done with this simple command in my desired directory:

```
git clone URL 
```

## 3. Editing the index.md

    For those wondering, there are plenty of markdown editors, or you can simply use a previewer and edit the file directly.

If you're not familiar with Markdown, you can find some documentation here:

[Markdown Cheat Sheet | Markdown Guide](https://www.markdownguide.org/cheat-sheet/)

## 4. Editing the module01.md

    Once I finished with the index page, I followed the same procedure in order to explain the different steps.

## 5. Compress images

    In order to avoid slow image loading, it is important to compress them so that they become web optimized and therefore load way faster.

You can simply compress your JPEG pictures but, I'll convert them to WebP format, which is a format specially designed for web images.

I'll use a free and open source software called [ImageMagick]([https://imagemagick.org](https://imagemagick.org)).

You can easily convert the images with the following command:

```
convert input.jpeg output.webp
```

    Don't hesitate to look up for documentation about ImageMagick, as it is a very powerful piece of software.

## 6. Making changes thought commits

    A git commit is a way of documenting a change for one or multiples files.

This allows others contributors or even the author to understand and or remember what has been done in the past.

A commit is done in the following way:

```
git commit -m "Your message"
```

Pay attention to the fact that if you were to add new untracked files, you'd need to add them into the local tracking list.

```
git add path
```

where path is the path to the file.

## 7. Pushing changes to the remote git repository

It is important to note that at first, a commit is a local change.

Once you committed one or multiples times, you can push your local changes to the remote repository.

This is done with the help of the following command:

```
git push
```
