# 4. CNC Miller Tool

    CNC Milling aka Computer Numerical Controll Milling is a type of tool that performs milling a part on a medium based on instructions given from a computer software. It allows for automatic and extremely precise machinning.

## 1. Safety precautions

    With power comes great responsibility. When using a CNC Miller, a number of precautions must be taken to avoid accidents and injuries.

    These are the general safety precautions. Some CNCs are more accident proof than others. It is important to know the machine you're working with. Use your common sense.

1. Wear protective equipment including:
   
   - safety goggles to avoid chips and debris projections from getting into your eyes.
   
   - ear protection as CNC milling is a noisy process especially if a vaccuum cleaner is hooked to the milling head.
   
   - gloves to avoid getting injured with splinters or metal chips.
   
   - dust mask to avoid breathing in any airborne particles created while milling a part. Some materials could generate small particles.

2. Secure the workpiece. It is important to ensure the workpiece is securely fastened to the milling table to prevent it from moving during the milling process. This could lead to the piece being detached while milling and cause injuries if there's no protective box around the CNC miller.

3. Stay focused and attentive when using a CNC miller, it is important to remain focused and pay attention to the machine at all times. Avoid distractions and never leave the machine unattended during operation in order to be able to stop the machine in case something goes wrong during the process.

4. Never override safety features, modern CNC millers have built-in safety features that are designed to prevent accidents and injuries. Do not attempt to override these safety features, as doing so could result in serious injury or even death (for the big industrial CNC millers).

5. Stay away from the cutter while operating and never stick your fingers next to the miller (if the CNC miller is not boxed) as this can obviously lead to serious injuries.

## 2. Designing the 3D model

    In this part we'll design a 3D model of the piece we want to CNC mill. This step is crucial as this is what will be milled on the medium.

This process is similar to the process of designing a 3D model for 3D printing. For more information read the *3. Modeling the part* section from the [3D printing module](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/fabzero-modules/module03/).

You can use any 3D or CAD software (Blender, FreeCAD, OpenSCAD, Autodesk, Fusion360, etc) for this step. Feel free to use the software of your choice and the one you're the most familiar with as long as you export your 3D model in formats such as: obj, stl or any other import format that is supported by Fusion360.

I will use FreeCAD to design the model since I can make it parametric right from the beginning.

    I personally decided to make a decoration element. I will design the logo of one of my personal projects.

Here are few screenshots of the modelling process:


![rad_1.webp](images/module04/rad_1.webp){width=647px}

![rad_2.webp](images/module04/rad_2.webp){width=648px}

![rad_3.webp](images/module04/rad_3.webp){width=649px}

## 3. Configuring the tool path

    We'll use the Autodesk's Fusion360 propietary software as it's widely used by various machines (millers, lathes, sheet metal benders, etc) in the digital machining and fabrication field.

    The tool path as its name suggests is the actual path, route that the CNC will take in order to perform milling based on the 3D model shape.

Those are the different steps to follow in order to generate the tool path based on the cutter and on the 3D model input.

### 3.1. Importing the model in Fusion360

    This first step is to import our 3D model in Fusion360.

This can be done in the following way:

![import_mesh.webp](images/module04/import_mesh.webp){width=649px}

It's a good practice to center the model on the scene and then place it on the ground in the following way:

![import_mesh_settings.webp](images/module04/import_mesh_settings.webp){width=649px}

Pay attention to the units. In case your orientation is wrong you can invert it from this importing menu you see on the figure above.

### 3.2. Converting the model to Fusion360 solid format

    At this stage we only imported a mesh aka a polygonal 3D model. Fusion360 has a specific encoding and format for a 3D model. It has the advantage of being fully parametric and a vector format.
Fusion360 can generate its own 3D parametric and vectorized representation from a polygonal 3D model. We'll need to convert our mesh into what they call a *solid body*.

The conversions are done in the following way:

1) Generate the face groups
   
   The software needs to know which are the different faces or surfaces of the model.
   
   This can be done in the following way:
   
   ![generate_face_groups_fix.webp](images/module04/generate_face_groups_fix.webp){width=618px}
   
   Usually the default parameters work very well as long as you chose *precise* for the type. You can always test and tweak them depending on your mesh. 
   
   Each surface or face group is visually represented by a different color.

2) Convert the polygonal 3D model into the parametric representation based on the determined face groups.
   
   ![conversion.webp](images/module04/conversion.webp){width=619px}
   
   Select the model in the scene and then click *convert mesh*.
   
   Now we need to configure the type of conversion. Fusion360 supports 2 algorithms for converting the polygonal meshes into a solid. I recommend using the *Prismatic method* since it always gives the best result and it requires no manual fixes or adjustments.
   
   ![convert_mesh.webp](images/module04/convert_mesh.webp){width=619px}
   
   I personally like to choose the *parametric* method over the *base function* because it allows me to make changes or tweaks within Fusion360.
   
   And now we have our parametric solid.
   
   ![converted_solid.webp](images/module04/converted_solid.webp){width=618px}

### 3.3. Configuring the medium

    Now that we have a native Fusion360 model, we can configure the medium.

Before doing that, we need to measure the 3 dimensions of our medium. If your medium is not parallelipipedic you'll have to take the largest values of its 3 dimensions as your medium would be inside a bounding box.

Once we have our dimensions we can create a new medium and specify its  dimensions. 

This can be done like this:

![medium_config.webp](images/module04/medium_config.webp){width=647px}

### 3.4. Adding cutters

    In order to generate a tool path and control the tool (in the case of more advanced CNC millers) we have to add and configure our cutters.

To add a cutter to the library we have to open the *tool library* menu.

![tool_lib.webp](images/module04/tool_lib.webp){width=648px}

![add_cutter.webp](images/module04/add_cutter.webp){width=648px}

I chose my type of cutter and then configured it by inputting the values from its specifications.

You can consult the specifications of your cutters or simply measure its dimensions and input them into the corresponding configuration panels.

![cutter_settings_1.webp](images/module04/cutter_settings_1.webp){width=649px}

![cutter_settings_2.webp](images/module04/cutter_settings_2.webp){width=649px}

We can find some cutter specifications [here](https://www.dehaye.com/).

### 3.5. Generating the tool path

    A tool path can be as complex as necessary. A tool path can be splitted into different passes.

This is the part where we have to be carefull for obstacles, clearences, finishing passes and to avoid stress on the overall machine (CNC, miller, cutter).

We need to first define our passes by selecting the face we want to machine and then  choosing a pass type from the drop menu.

![adding-adaptive-pass.webp](images/module04/adding-adaptive-pass.webp){width=647px}

Here go with an 2D adaptive pass. It optimizes the path but leaves irregular machining marks which doesn't matter at all for me.

Now we need to configure the pass and more precisely choosing the tool we configured earlier and this is where we have to be careful:

![pass_select_tool.webp](images/module04/pass_select_tool.webp){width=429px}

I also disabled the lubrication since the CNC miller I'm using isn't equiped with a lubrication and cooling system.

Now we can configure the actuall pass settings:

![pass_settings.webp](images/module04/pass_settings.webp){width=386px}

Bidirectional allows for the cutter to go front and back while cutting. This has the effect of speeding the milling process. You can enable multiple depth is a fancier way of diving into the media little by little. This has the advantage to cut the load on the tool. Disable the extra-thickness / allowance only if you don't plan to add a smoothing pass.

Now we can configure the linking settings of the pass:

![pass-linking-settings.webp](images/module04/pass-linking-settings.webp){width=649px}

The ramp setting allows for progressively enterring the medium to avoid axial stress on the miller as well as on the cutter tip. The helicoidal movement decrease the amount of stress on the tool. It is very usefull for drilling too.

And here is the 2D contour pass:

![add_contour_pass.webp](images/module04/add_contour_pass.webp){width=649px}

The settings are very similar to the ones of the 2D adaptive pass so I won't go trough them again.

    Since I want to cut all the way trough the medium with the contour pass, I'll add tabs so that the cut piece doesn't get fully detached as this could damage the machine and my piece. The tabs are portions where the miller won't cut. The tabs will act as holders beweetn the peice we're milling and the rest of the medium.

This is how you can enable tabs on your piece:

![adding_tabs.webp](images/module04/adding_tabs.webp){width=648px}

You can place them automaticall or manually with the help of the cursor:

![create_tabs.webp](images/module04/create_tabs.webp){width=649px}

### 3.6. Simulate the milling process

    After configuring the entire tool path we should end up with something like this:

![final_configuaration.webp](images/module04/final_configuaration.webp){width=648px}

    While making the tool path and after finishing it, it is crucial to simulate the milling process in order to avoid wrong paths, hitting obstacles such as fixations or applying excessive forces on the cutter which can cause premature wear and shorten its lifespan or even damage the CNC miller.

    The simulation can be started in the following manner:

![start_simulation.webp](images/module04/start_simulation.webp){width=649px}

Now we can see the simualtion menu and play it:

![simulation.webp](images/module04/simulation.webp){width=649px}

Excessive forces are represented by the red indicators on the simulation bar.

    It is crucial to analyze the simulation carefully and adapt the tool path configuration to reduce those forces as much as possible as well as avoiding clearence issues or anything that could lead to a machining failure.

### 3.7. Exporting the GCode

    The GCode is the set of low level instructions for a machine. Those include the tool path aswell as all the types of actions a machine must execute. Every tool, in this case a CNC miller, has its own type of GCode. It is defined by the manufacturer.

We have to make sure we choose the right *preprocessor* for our specific CNC.

Choosing the corresponding preprocessor:

![export_gcode.webp](images/module04/export_gcode.webp){width=646px}

![simulation.webp](images/module04/export_gcode_2.webp){width=645px}

Generate the GCode and upload it on a USB stick bi clicking on *Post process button* in order to be able to transfer it on the computer that drives the CNC miller.

## 4. Setup the CNC Milling machine

    Now we can finally setup the CNC milling machine for milling the part.

### 4.1. Set the cutter

    First we need to set the first cutter that is used in the milling order defined by the tool path configuration. In my case I have a single tool.

To attach the tool we simply loosen the attachement 'nut and' and insert the cutter then tight the nut back.

### 4.2. Secure the medium on the table

    The chosen medium should be secured in place using screws or attachement points. Make sure you tool path won't hit those screews at attachements points. As mentioned earlier, you can define custom avoidance zones to avoid hitting anything and cause heavy damages to the machine as well as to the cutter.

### 4.3. Setup the axes

    We have to move the milling head where we want our origin to be. And then zero the corresponding axes. For the height axis, we have to move the CNC head in order to zero the height. This tool has a button and it's connected to the CNC. This is done by lowering the milling head (with the cutter inserted) until it hits the button. The button will stop the lowering movement automatically and then the height axis can be zeroed. This ensures height calibration based on the cutter stickout since the tool height can vary based upon cutter installation.

### 4.4. Upload the GCode

    Before uploading the GCode, don't forget to check the tool configuration to make sure the cutter stickout corresponds to the values from the cutter configuration in the software in the tool library.

## 5. Milling process the part

    Once the previous steps were carefully executed, we can start the milling process.

Pay attention to the milling process in case something unexpected happens. You should be able to quickly stop the machine in case something goes wrong.

Once the milling operation is done. Remove the piece by removing the screws or the attachements and inspect your piece afterwards.

Here is a short video clip from the milling process: 

<div>
	<video width="560" height="315" controls>
	  <source src="../images/module04/video_milling_process.mp4" type="video/mp4">
	  Your browser does not support the video tag.
	</video>
</div>

![milling_process.webp](images/module04/milling_process.webp){width=650px}

Here is my milled piece:

![export_gcode_2.webp](images/module04/export_gcode_2.webp){width=659px}

Now we need to cut the tabs. I used a cutting wheel on an angle grinder.

![result_2.webp](images/module04/result_2.webp){width=422px}