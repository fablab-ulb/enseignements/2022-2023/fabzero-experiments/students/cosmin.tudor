# 5. Group dynamics and final project

## 5.1. Project Analysis and Design

The main idea of this analysis is to identify the main problem and from it, all its causes and consequences. This is typically done with what's called a *problem tree*.

This was introduced to us with the help of this [video](https://www.youtube.com/watch?app=desktop&v=9KIlK61RInY).

The purpose of the problem tree is to visually analyze and understand the root causes of a particular problem or issue. It helps to identify the various factors that contribute to a problem and helps establishing a clear cause-and-effect relationship between them. That's quite a powerful tool since it allows for better visualization as well as improved problem-solving processes.

### 5.2.1. Group formation

    At the beginning of the group formation process, each student was asked to bring an item related to a subject they care about. The students then shared their items with the rest of the group and placed them on the floor. They walked around, analyzing the items to find others related to theirs. This exercise helped in forming groups with members who shared similar interests.

    I personally brought an AT Tiny microcontroller because I love electronics and programming. I really hate closed-source and proprietary hardware and software since it often encourage planned obsolescence, full control over a technology, is a freedom killer and contribute to issues such the generation of e-waste. The items I found related to my microcontroller were the computer CPU, phone, a Raspberry Pi and headphones.

    We then formed a group with the owners of those items, as well as a few other people, and discussed what our personal items represented to us and the ideas behind them. After the discussion, we were able to form groups.

Our group is composed of:

- Cosmin Alexandru Tudor (myself)

- Patrik Dezséri

- Louis Vanstappen

- Dimitri Debauque

### 5.2.2. Brainstorming on project problems

    Once the groups were formed, they brainstormed on the problems related to their subjects. My group and I  identified e-waste, precious metal waste, proprietary and closed-source technologies, and low degree of repairability as major problems.

We also participated in a competition with other groups where we all wrote down as many words related to our  subjects in three minutes, and our group won with a total of 70 words.

## 5.3. Group dynamics

    The groups chose tools that would facilitate effective decision-making and communication.

![communication_tools.webp](images/module05/communication_tools.webp){width=648px}

### 5.3.1 Voting

    Voting is a common tool used in groups to make decisions. We opted for 2 types of votes:

- A vote without objection which occurs when a decision is made by a group even if not everyone is in favor of it, but there are no clear objections.

- A majority judgment which is a voting method in which participants rate a proposal on a scale of 0 (completely against) to 10 (fully in favor), with all intermediate values also being possible.

### 5.3.2. The weather

    The weather is a tool where members raise their hands proportionally to communicate their level of motivation or energy. It is a quick and easy method that requires no talking and is useful for determining the group's overall mood.

### 5.3.3. What's up Doc

    What's Up Doc is a tool used to prevent behavior inference. It allows members to express how they feel, whether they are having a good or bad day, to help others understand their emotional state and avoid misunderstandings.

## 5.4 Problem and objective of your group project

Our main problem was the *e-waste*. The* e-waste* also known as *electronic waste* is esentially the obsolete or discarded pieces of electronics. Those are usually very hard to process and recycle due to their nature.

We came with the follwing problem tree:

![communication_tools.webp](images/module05/problem-tree.webp){width=670px}

As well as the following objective tree:

![communication_tools.webp](images/module05/objective-tree.webp){width=664px}
