# 2. Computer Assisted Design (CAD)

    The task of this week is to design a parametric 3D model of a [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks) kit part that will later be 3D printed.

Here's a preview of what needs to be achieved:

![model-preview.webp](images/module02/model-preview.webp){width=636px}

## 1. Setup

    There are plenty of CAD pieces of software but, I personally like to use open source software since I can easily modify them and have the guaranty that they are not driven by a company of which the main goal is to make profit. For this particular task, due to time constraints, I chose FreeCAD. Usually I'd go with OpenSCAD.

## 2. Basic CAD principle

    CAD modeling is about making complex shapes out of primitives such as lines, vertices, circles, etc. and put them in relations ones with the others with the help of constraints such as tangents, parallelism, horizontality in order to be able to achieve complex shapes.

The idea is to put your shape in a steady state or a partial steady state.

## 3. Modeling the part

### 3.1. Modeling technique

    In the 3D modeling world, there are many techniques of modeling for all kinds of situations. Some are very generic, others are very specific.

In our case, we can use a technique that consists in modeling the base in 2 dimensions (the scheme), which is a 2D projection of one of the faces of the model on a plane and then extrude that 2D shape in order to add the 3rd dimension and make the model three-dimensional.

### 3.2. Choosing the parameters

    Since we want our model to be parametric, we need to spot the required parameters.

Here are the ones that I spotted:

### 3.3. Setting the parameters in FreeCAD

    In order to make our model parametric, we need to define our parameters first. This is done through a *spreadsheet* from the tooling menu. Its purpose is to basically have some variables, that are essentially our parameters, and be able to assign values to them without editing the actual model by hand.

![spreadsheet.webp](images/module02/spreadsheet.webp)

This is how I defined my parameters as well as their initial values:

![parameters.webp](images/module02/parameters.webp)

### 3.4. Modeling the base (2D shape)

    As mentioned earlier, the idea is to start by primitives and make them more and more complex until reaching the desired shape.

I started by a slot.

![mod-1.webp](images/module02/mod-1.webp)

Then, I'll assign my first parameter, which in this case is the distance between the two centers composing the slot. This is done in the following manner:

![mod-2.webp](images/module02/mod-2.webp)

After a little bit of work, we obtain the final parametric shape:

![mod-3.webp](images/module02/mod-3.webp){width=648px}

### 3.5. Extruding the base

    Now that our 2D base is done. We can add a third dimension by extruding that base using the *pot* tool from the task list.

![mod-5.webp](images/module02/mod-5.webp){width=648px}

I also provided a parameter for the extrusion, representing here the thickness of the part.

### 3.6. Bevels and chamfers

    In order to reinforce and provide a lock mechanism for the clips, some chamfers and bevels are required.

Before applying them, we need to select the edges that we desired the bevels or the  chamfers to be applied on.

We can then add the desired modifier from the *edge tools*.

![mod-6.webp](images/module02/mod-6.webp){width=643px}

Here, I specified again a parametric value for the chamber and bevels.

![mod-7.webp](images/module02/mod-7.webp){width=646}

### 3.7. Final result

    Here's the finished 3D model:

![model-preview.webp](images/module02/model-preview.webp){width=646}

## 4. Licensing with Creative Commons

    In order to allow other people to use our work, we need to give them the right to use it.

This can be done with the help of a license. We'll use a [Creative Commons](https://creativecommons.org) license.

In order to license our work, we only need to click on *Share your work* section on the official website and specify the conditions under which our work can be used, then either paste the HTML code on your personal website or the text into a text file. When using a text file, this file must be distributed along your work.

## 5. Download my 3D models files

Here they are:

![preview_model_1.webp](images/module02/preview_model_1.webp)

![preview_model_2.webp](images/module02/preview_model_2.webp){width=614}

The dimensions of the models are:
70mm * 6mm * 8mm

My 3d models can be downloaded her [here](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/cosmin.tudor/-/blob/main/docs/fabzero-modules/files/flexible-link/flexible-link.zip) .
