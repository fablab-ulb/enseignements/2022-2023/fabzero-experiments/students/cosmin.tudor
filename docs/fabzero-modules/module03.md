# 3. 3D Printing

Now that our 3D models are ready, we can proceed to the 3D printing step.

## 1. Choosing the material

    There are many different plastics, polymers, all having their specific characteristics, that can be used to 3D print with.

Our choice is PLA, which stands for *Polylactic Acid*. It is a bioplastic, and it is produced from renewable resources,  which makes PLA very affordable. It's one of the most widely used plastics in 3D printing. PLA has also the advantage of being annealed, which makes it heat-resistant. PLA is also a high strength and flexible plastic that  doesn't produce many fumes while being melted for 3D printing.

Source: [Polylactic acid - Wikipedia](https://en.wikipedia.org/wiki/Polylactic_acid)

## 2. Establishing values for the parameters

    Due to coordination, organization issues and time scheduling constraints, I decided to use the standard Lego measurements for the models in order to ensure compatibility with. 

## 3. 3D Printing

### 3.1. Exporting the model

    Before 3D printing our models, we first need to export them in STL or in OBJ format. We went with STL [explain why].

### 3.2. Generating the G Code

In order for a 3D printer to be able to move its head accordingly to the model shape, it needs some instructions. These are called G Code. It can be generated from our STL model that we previously exported.

This is also where we specify the material we will be using,  as well as the precision and the density we desire.

The fully detailed documentation regarding printing can be found [here]:

### 3.3. Preparing the 3D printer

#### 3.3.1. Cleaning

    Before printing, it is necessary to clean the pad. It must be grease free in order to prevent the model form unsticking during the printing operation, which would result in printing failure.

#### 3.3.2. Calibration

    It is good practice to run a quick test before printing to make sure the printer is calibrated. If it is not, then the calibration procedure needs to be launched from the printer menu.

### 3.4. Printing

    Once the previous steps are completed, the SD card containing the G Code can be inserted. Finally, the printing can be launched from the printer menu.

    My group and I have decided that, due to delay issues, it was better to assemble all our models into a single G Code and print them at once.

This saved us quite a bit of time.

## 4. Discussing together

![printed-part.webp](images/module03/printed-part.webp)

    Once the printing was done, we checked and discussed  the quality of our models and their flexibility. It allowed us to get a first feeling of how that PLA material was like.
