# About me

<img title="" src="images/profile.webp" alt="" width="300">

Hi! I'm Cosmin Alexandru Tudor. I'm 22 years old, and I'm a computer science student at ULB.

# My hobbies

I have a lot of different hobbies: computer programming (mostly low level), computer graphics aka CG and CGI, music composition, analogic and digital electronics, metal working, restoring old machines, inventing stuff, math, physics (mostly Newtonian physics), a bit of chemistry, and many more.

# Some of my main personal projects

#### C/C++ dialect

Since C and C++ are 2 very old programming languages. I've decided to make my own dialect in order to implement some features and improve the language a bit. The idea is to improve the overall structure, the compilation process and introduce a personal standard that is cleaner.

#### Game Engine

My main project is a very ambitious one. I'm writing an advanced AAA fully featured and optimized modern 3D game engine from scratch. The game engine is fully written in C/C++ and ASM. It uses Vulkan as a render API. No third party library is used in the process, everything is written from scratch.

#### Proprietary audio-video codec reverse engineering

That's another tricky long term project. Reverse engineering proprietary software is never an easy task. I'm currently trying to reverse engineer some lossy audio-video codec that has a very high compression rate. Both the encoder and the decoder must be reverse engineered as long as the codec specifications.

#### Old discontinued game reverse engineering

Another quite ambitious project was to reverse engineer some survival multiplayer game that was released in 2013. The client is almost fully reverse engineered. The progress is around 90%. On the other hand, the server is not done yet.

#### Obsolete ignition from a 1968 gasoline engine

Since I'm a big fan of vintage machines. I have a huge collection of old engines and small tractors produced between the 40' and the 90'. One of them had its electronic ignition broken. I had to remove the whole circuit from the resin, reverse engineer it and modify it to improve it, then build it with better modern electronic components. I started this project during the pandemic. It is still on hold due to some difficulties of finding an adequate ferrite core and a high temperature SRCs.

#### Designing and fabricating small tractors

I love to build tools and machines. I restore and build small tractors out of car parts. Unfortunately, since I don't have a lathe and a miller, yet it is very expensive to make the parts. It takes me a lot of time. The design is usually done trough CAD software.